const video_wrapper = document.querySelector(".video-wrapper"),
  video_container = document.querySelector(".video-container"),
  videoData = JSON.parse(localStorage.BDVideo);

console.dir(JSON.parse(localStorage.BDVideo).length);

if (JSON.parse(localStorage.BDVideo).length === 0) {
  video_wrapper.innerHTML = "";
  video_wrapper.insertAdjacentHTML("beforeend", "<h2>Поки відео немає</h2>");
} else {
    video_container.insertAdjacentHTML(
      "beforeend",
      videoData
        .map((el) => {
          return `<div class="video">
        <video src="${el.videoLink}" controls>
            
        </video>
    </div>`;
        }).join("")
    );
}
