import { showModal,closeModal,createElement } from "./function.js";

const tbody = document.querySelector("tbody"),
    modalBtnClose = document.getElementById("close"),
    modalBtnSave = document.getElementById("save"),
    editElement = document.querySelector(".edit-element")
     
const store = JSON.parse(localStorage.BDStore);



// Global function start
function editCard(obj = {}, typeStore) {
    if (typeStore.includes("store")) {
        editElement.innerHTML = "";
        editElement.dataset.key = obj.id;
        const { keywords, productPrice, productDescription, productImage, productName, productQuantity } = obj;
        const inputs = [
          createElement("input", undefined, keywords.join(), "Гарячі слова"),
          createElement("input", undefined, productPrice, "Віртість продукту","number"),
          createElement("input",undefined,productDescription,"Опис продукту"),
          createElement("input", undefined, productImage, "Картинка продукту"),
          createElement("input", undefined, productName, "Назва продукту"),
          createElement("input",undefined,productQuantity,"Кількість продукту","number"),
        ];
        inputs.forEach((el) => {
            el.addEventListener('change', () => {
                console.log("change");
            })
        });

        editElement.append(...inputs)
    }
    
}
// Global function end


modalBtnClose.addEventListener("click", closeModal);

function createElementTable (store) {
    return store.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.productName}</td>
            <td title="При настиску сортувати.">${el.productQuantity}</td>
            <td title="При настиску сортувати.">${el.productPrice} грн.</td>
            <td class="edit" data-index=${i}>&#128397;</td>
            <td>${el.status ? "&#9989;" : "&#10060;" }</td>
            <td>${el.date}</td> 
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(store));

const edits = document.querySelectorAll(".edit");

edits.forEach((el) => {
    el.addEventListener("click", (el) => {
        showModal();
        editCard(store[el.target.dataset.index],document.location.pathname)
    })
})


modalBtnSave.addEventListener('click', () => {
    const inputs = document.querySelectorAll(".edit-element input");
    console.log(inputs);
    console.log(editElement.dataset.key);
})

  