import {
  showModal,
  closeModal,
  createInputsForModal,
  generationId,
  createElement,
} from "./function.js";

const btnShowModal = document.querySelector(".add"),
  modalBtnClose = document.getElementById("close"),
  modalBtnSave = document.getElementById("save"),
  select = document.getElementById("select"),
  formInfo = document.querySelector(".form-info"),
  store = [
    "Назва продукту",
    "Вартість продукту",
    "Посилання на зображення",
    "Опис продукту",
    "Ключеві слова(Розділяти комою)",
  ],
  restoration = [
    "Назва страви",
    "Вага страви(по компонентам)",
    "Інгрідієнти",
    "Ціна",
    "Посилання на зображення",
    "Ключеві слова (Розділяти комою)",
    "Загальна вага",
  ],
  video = [
    "Посилання на відео",
    "Назва відео",
    "Жанр відео",
    "Країна походження",
    "Актори(Розділяти комою)",
    "Опис",
    "Рік",
  ];

let typeCategory = null;

select.addEventListener("change", (e) => {
  typeCategory = select.value;
  if (typeCategory === "Магазин") {
    formInfo.innerHTML = "";
    formInfo.insertAdjacentHTML("beforeend", createInputsForModal(store));
  } else if (typeCategory === "Відео хостинг") {
    formInfo.innerHTML = "";
    formInfo.insertAdjacentHTML("beforeend", createInputsForModal(video));
  } else if (typeCategory === "Ресторан") {
    formInfo.innerHTML = "";
    formInfo.insertAdjacentHTML("beforeend", createInputsForModal(restoration));
  } else {
    console.error("Жоден з пунктів не валідний");
    return;
  }
});

btnShowModal.addEventListener("click", showModal);
modalBtnClose.addEventListener("click", closeModal);

modalBtnSave.addEventListener("click", () => {
  const [...inputs] = document.querySelectorAll(".form-info input");
  const objStore = {
    id: "",
    status: false,
    productName: "",
    productPrice: 0,
    productImage: "",
    productDescription: "",
    productQuantity: 0,
    keywords: [],
  };

  if (typeCategory === "Магазин") {
    objStore.id = generationId();
    inputs.forEach((input) => {
      if (input.value.length > 3) {
        switch (input.dataset.type) {
          case "Назва продукту":
            objStore.productName = input.value;
            break;
          case "Вартість продукту":
            objStore.productPrice = parseFloat(input.value);
            break;
          case "Посилання на зображення":
            objStore.productImage = input.value;
            break;
          case "Опис продукту":
            objStore.productDescription = input.value;
            break;
          case "Ключеві слова(Розділяти комою)":
            objStore.keywords.push(...input.value.split(","));
        }
        input.classList.remove("error");

        inputs.forEach((el) => {
          el.value = "";
        });
      } else {
        input.classList.add("error");
        return;
      }
    });
    if (objStore.productQuantity <= 0) {
      objStore.status = false;
    } else {
      objStore.productQuantity = true;
    }
    objStore.date = new Date();
    const store = JSON.parse(localStorage.BDStore);
    store.push(objStore);
    localStorage.BDStore = JSON.stringify(store);
  }
});

modalBtnSave.addEventListener("click", () => {
  const [...inputs] = document.querySelectorAll(".form-info input");
  const objRestoration = {
    id: "",
    productName: "",
    productWeight: "",
    ingredients: "",
    price: 0,
    productImageUrl: "",
    keywords: [],
    Weight: 0,
    stopList: true,
    ageRestrictions: false,
    like: 0,
  };
  const objVideo = {
    id: 0,
    videoLink: "",
    videoName: "",
    videoGenre: "",
    videoCountry: "",
    videoActors: [],
    videoDescription: "",
    videoYearReleased: 0,
  };

  if (typeCategory === "Ресторан") {
    objRestoration.id = generationId();
    inputs.forEach((el) => {
      if (el.value.length > 3) {
        switch (el.dataset.type) {
          case "Назва страви":
            objRestoration.productName = el.value;
            break;
          case "Вага страви(по компонентам)":
            objRestoration.productWeight = el.value;
            break;
          case "Інгрідієнти":
            objRestoration.ingredients = el.value;
            break;
          case "Ціна":
            objRestoration.price = parseFloat(el.value);
            break;
          case "Посилання на зображення":
            objRestoration.productImageUrl = el.value;
            break;
          case "Ключеві слова (Розділяти комою)":
            objRestoration.keywords.push(...el.value.split(","));
            break;
          case "Загальна вага":
            objRestoration.Weight = parseFloat(el.value);
            break;
        }
        el.classList.remove("error");
      } else {
        el.classList.add("error");
        return;
      }
    });
    objRestoration.date = new Date();

    const restoration = JSON.parse(localStorage.BDRestoration);
    restoration.push(objRestoration);
    localStorage.BDRestoration = JSON.stringify(restoration);
  }
  if (typeCategory === "Відео хостинг") {
    objVideo.id = generationId();
    inputs.forEach((el) => {
      if (el.value.length > 3) {
        switch (el.dataset.type) {
          case "Посилання на відео":
            objVideo.videoLink = el.value;
            break;
          case "Назва відео":
            objVideo.videoName = el.value;
            break;
          case "Жанр відео":
            objVideo.videoGenre = el.value;
            break;
          case "Країна походження":
            objVideo.videoCountry = el.value;
            break;
          case "Актори(Розділяти комою)":
            objVideo.videoActors.push(...el.value.split(","));
            break;
          case "Опис":
            objVideo.videoDescription = el.value;
            break;
          case "Рік":
            objVideo.videoYearReleased = parseFloat(el.value);
            break;
        }
        el.classList.remove("error");
      } else {
        el.classList.add("error");
        return;
      }
    });
    objVideo.date = new Date();

    const video = JSON.parse(localStorage.BDVideo);
    video.push(objVideo);
    localStorage.BDVideo = JSON.stringify(video);
  }
  inputs.forEach((el) => {
    el.value = "";
  });
});

//                               Функционал кнопки экспортировать НАЧАЛО

document.getElementById("exportJSON").addEventListener("click", () => {
  const modalBox = document.createElement("div"),
    modal = document.createElement("pre"),
    modalBtnClose = document.createElement("button");
  modalBox.append(modal);
 

  modalBox.classList.add("container-modal");
  modal.classList.add("modal");

  let data = "";
  console.log(data + localStorage.BDStore);

  if (localStorage.BDStore.length > 0) {
    data += localStorage.BDStore;
  }
  if (localStorage.BDRestoration.length > 0) {
    data += localStorage.BDRestoration;
  }
  if (localStorage.BDVideo.length > 0) {
    data += localStorage.BDVideo;
  }

  if (data === 0) {
    console.log("Поки що тут пусто");
  }

  modalBox.classList.add("active")

  document.body.append(modalBox);
  modal.innerHTML = data;

  modalBtnClose.setAttribute("type", "button");
  modalBtnClose.innerText = "Закрити!";
  modalBtnClose.classList.add("close-btn");
  modal.append(modalBtnClose);

  modalBtnClose.addEventListener('click', () => {
    modalBox.classList.remove("active")
  })
});

//                                   Функционал кнопки экспортировать КОНЕЦ

//                                  Данные по URL НАЧАЛО

document.getElementById("getServerData").addEventListener('click', () => {
  console.log(getAjax()); 
  // getFetch();
})

async function getAjax() {
  const iconLoader = loader();
  try {
    const req = new XMLHttpRequest();
    req.open("GET",URL)
    req.send();
    req.addEventListener('readystatechange', () => {
      if (req.readyState === 4 && req.status >= 200 && req.status < 300) {
        iconLoader.remove();
        return req.responseText;
      } else if (req.readyState === 4) {
        throw new Error("Помилка при запиті")
      }
    })
  } catch (error) {
    console.error(error);
    
  }
}

function loader() {
  const loaderBox = document.createElement("div"),
    loader = document.createElement("span"),
    style = document.createElement("style");
  style.innerHTML = `.loader{
    display: block;
    position: relative;
    height: 20px;
    width: 140px;
    background-image: 
    linear-gradient(#FFF 20px, transparent 0), 
    linear-gradient(#FFF 20px, transparent 0), 
    linear-gradient(#FFF 20px, transparent 0), 
    linear-gradient(#FFF 20px, transparent 0);
    background-repeat: no-repeat;
    background-size: 20px auto;
    background-position: 0 0, 40px 0, 80px 0, 120px 0;
    animation: pgfill 1s linear infinite;
  }
  
  @keyframes pgfill {
    0% {   background-image: linear-gradient(#FFF 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0); }
    25% {   background-image: linear-gradient(#153df0 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0); }
    50% {   background-image: linear-gradient(#153df0 20px, transparent 0), linear-gradient(#153df0 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0); }
    75% {   background-image: linear-gradient(#153df0 20px, transparent 0), linear-gradient(#153df0 20px, transparent 0), linear-gradient(#153df0 20px, transparent 0), linear-gradient(#FFF 20px, transparent 0); }
    100% {   background-image: linear-gradient(#153df0 20px, transparent 0), linear-gradient(#153df0 20px, transparent 0), linear-gradient(#153df0 20px, transparent 0), linear-gradient(#153df0 20px, transparent 0); }
  }
  `;

  loaderBox.classList.add("container-modal", "active");
  loader.classList.add("loader");

  loaderBox.append(loader);
  document.head.append(style);
  document.body.append(loaderBox);
  return loaderBox;
}
