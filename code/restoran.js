const tbody = document.querySelector("tbody");
const restoration = JSON.parse(localStorage.BDRestoration);

console.log(tbody);

function createElementTable(arr) {
  return arr
    .map((el, i) => {
      console.log(el.date);
      return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.productName}</td>
            <td title="При настиску сортувати.">${el.productWeight}</td>
            <td title="При настиску сортувати.">${el.productPrice} грн.</td>
            <td class="edit" data-index=${i}>&#128397;</td>
            <td>${el.stopList ? "&#9989;" : "&#10060;"}</td>
            <td>${el.date}</td> 
            <td>&#128465;</td>
        </tr>
        `;
    })
    .join("");
}

tbody.insertAdjacentHTML("beforeend", createElementTable(restoration));
