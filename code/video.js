const tbody = document.querySelector("tbody"),
    video = JSON.parse(localStorage.BDVideo);

function createElementTable(arr) {
    return arr.map((el,i) => {
        return `
        <tr>
            <td>ID: ${el.id}</td>
            <td>${el.videoName}</td>
            <td title="При настиску сортувати.">${el.date}</td>
            <td title="При настиску сортувати."><a href="${el.videoLink}" target="_blanck">${el.videoLink}</a></td>
            <td class="edit" data-index=${i}>&#128397</td>
            <td>&#128465</td>
        </tr>
        `;
    }).join("")
}

tbody.insertAdjacentHTML("beforeend",createElementTable(video))

