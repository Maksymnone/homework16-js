// відображення сторінки авторизації
export function isAuthorization() {
  if (document.location.pathname.search("authorization") !== -1) {
    return;
  }
  if (!localStorage.isAuthorization) {
    document.location = "/authorization";
  }
}

// Перевірка для логіну та поролю
export function validate(regExp, val) {
  return regExp.test(val);
}

// відображення(закривання) модального вікна
const modalWin = document.querySelector(".container-modal");
const showModal = (e) => modalWin.classList.add("active");
const closeModal = (e) => modalWin.classList.remove("active");

// наповнення модального вікна інпутами
function createInputsForModal(arr = []) {
  if (!Array.isArray(arr)) {
    throw new Error("Прийшов не масив!")
  }
  return arr
    .map((input) => {
      const id = generationId();
      return `<div class="input-line"><label for="${id}">${input}</label><input data-type="${input}" id="${id}" type="text"></div>`;
    })
    .join("");
}


export function editInputStore () {
  
}


// фyнкція генерування id
function generationId() {
  const a = ["q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m","й","ц","у","к","е","н","г","ш","щ","з","х","ї","ф","і","в","а","п","р","о","л","д","ж","є","ґ","я","ч","с","м","и","т","ь","б","ю",
  ];
  const b = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  let id = "";
  for (let i = 0; i < 10; i++) {
    id += `${a[Math.floor(Math.random() * a.length + 1)]}${
      b[Math.floor(Math.random() * b.length)]
    }`;
  }
  return id;
}

const createElement = (e = "div", c ="" , v = "", p="",t="text") => {
  const element = document.createElement(e);
  if (c !== "") {
    element.classList.add(c);
  }
  if (element.tagName === "INPUT") {
    const div = document.createElement("div");
    const label = document.createElement("label");
    element.type = t;
    const id = generationId();
    element.value = v;
    label.innerText = p;
    label.setAttribute("for", id)
    element.id = id;
    label.append(element)
    div.append(label)
    return div;
  } else {
    element.innerText = v;
    return element;
  }
  
};

export { showModal, closeModal, createInputsForModal, generationId,createElement };
