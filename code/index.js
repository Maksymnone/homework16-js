import { isAuthorization, validate } from "./function.js";
import { login, password } from "./authorization.js";
// isAuthorization();

console.log(login);
console.log(password);

if (!localStorage.BDStore) {
  localStorage.BDStore = JSON.stringify([])
}
if (!localStorage.BDVideo) {
  localStorage.BDVideo = JSON.stringify([]);
}

if (!localStorage.BDRestoration) {
  localStorage.BDRestoration = JSON.stringify([])
}


try {
const elLogin = document.querySelector("input[data-type='login']"),
  elPassword = document.querySelector("input[data-type='password']"),
  elBtn = document.querySelector("button[data-type='btn-go']");

elLogin.addEventListener("change", (e) => {
  if (validate(new RegExp(`^${login}$`), e.target.value)) {
    elLogin.classList.remove("error");
    elLogin.classList.add("valid");
  } else {
    elLogin.classList.remove("valid");
    elLogin.classList.add("error");
  }
});

elPassword.addEventListener("change", (e) => {
  if (validate(new RegExp(`^${password}$`), e.target.value)) {
    elPassword.classList.remove("error");
    elPassword.classList.add("valid");
  } else {
    elPassword.classList.remove("valid");
    elPassword.classList.add("error");
  }
});


elBtn.addEventListener('click', () => {
    if (validate(new RegExp(`^${login}$`),elLogin.value) && validate(new RegExp(`^${password}$`),elPassword.value)) {
        localStorage.isAuthorization = true;
        document.location = "/";
    } else {
        elPassword.classList.add("error");
        elLogin.classList.add("error");
    }
})

} catch (error) {
    console.error(error);
    
}
